*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time=0]


[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="35" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]

[chara_config ptext="chara_name_area"]

;————————ここから————————

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]

;キャラ２定義
[chara_new name="hi" storage="chara/hi/hi01.png" jname="hi"]

;キャラ１表情
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]

;キャラ２表情
[chara_face name="hi" face="normal" storage="chara/hi/hi01.png" jname="hi"]
[chara_face name="hi" face="komari" storage="chara/hi/hi02.png" jname="hi"]
[chara_face name="hi" face="happy" storage="chara/hi/hi03.png" jname="hi"]
[chara_face name="hi" face="doki" storage="chara/hi/hi04.png" jname="hi"]
[chara_face name="hi" face="sad" storage="chara/hi/hi05.png" jname="hi"]
[chara_face name="hi" face="jitome" storage="chara/hi/hi06.png" jname="hi"]
[chara_face name="hi" face="angry" storage="chara/hi/hi07.png" jname="hi"]
[chara_face name="hi" face="cry" storage="chara/hi/hi08.png" jname="hi"]

;背景
[image layer=0 folder="bgimage" name="gaikan" storage="gaikan_hiru.jpg" visible=true top=0 left=0 width=1200 height=675]

;メニューボタン
@showmenubutton

[mask_off time=1000]
[wait time=200]

[chara_mod name="akane"  face="normal"]
[chara_mod name="hi"  face="normal"]
[chara_show name="akane" left=100 top=60]
[chara_show name="hi" left=480 top=110]
[playbgm storage="dozikkomarch.ogg"]

@layopt layer=message0 visible=true

#まどか
……………………[l]

[cm]

@layopt layer=message0 visible=false

[anim name=akane left="+=150" time=500 effect="easeInSine"]
[wait time=1000]

[anim name=akane left="+=10" time=100]
[anim name=hi left="+=5" time=100]
[wait time=100]
[anim name=akane left="-=10" time=100]
[anim name=hi left="-=5" time=100]
[wait time=100]
[anim name=akane left="+=10" time=100]
[anim name=hi left="+=5" time=100]
[wait time=100]
[anim name=akane left="-=10" time=100]
[anim name=hi left="-=5" time=100]
[wait time=200]

[wa]

@layopt layer=message0 visible=true

#まどか
……つんつん[l]

[cm]

@layopt layer=message0 visible=false

[chara_mod name="hi"  face="angry"]
[anim name=hi top="+=10" effect="easeInOutQuint" time=300]
[wait time=300]
[anim name=hi top="-=10" effect="easeOutQuint" time=300]
[wait time=400]

[wa]

@layopt layer=message0 visible=true

[font size=30]

#ほむら
ぎゃ～！！[l]

[cm]

[resetfont]
#ほむら
何するのよ！！！[r]
足しびれてたのに～！！！！[l]

[cm]

@layopt layer=message0 visible=false

[chara_mod name="akane"  face="happy"]
[anim name=akane left="-=150" time=500 effect="easeInSine"]
[wait time=1000]

[wa]

@layopt layer=message0 visible=true

#まどか
あはは、ごめんごめん[l]

[cm]

@layopt layer=message0 visible=false

[chara_mod name="akane"  face="normal"]
[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[wait time=100]
[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]

[wait time=200]

[wa]

@layopt layer=message0 visible=true

#まどか
ねえねえ、これは何？[l]

[cm]

[chara_mod name="hi"  face="normal"]
#ほむら
ああ、それね。焼き芋よ[l]

[cm]

[chara_mod name="akane"  face="happy"]
#まどか
へ～、おいしそうだね！[l]

[cm]

[chara_mod name="hi"  face="happy"]
#ほむら
今、焼くから待ってなさい[l]

[cm]

@layopt layer=message0 visible=false

[chara_mod name="akane"  face="angry"]
[anim name=akane left="-=10" time=100]
[wait time=100]
[anim name=akane left="+=10" time=100]
[wait time=100]
[anim name=akane left="-=10" time=100]
[wait time=100]
[anim name=akane left="+=10" time=100]

[wait time=400]

[wa]

@layopt layer=message0 visible=true

#まどか
……[l]

[cm]

[chara_mod name="hi"  face="doki"]
#ほむら
落ち着かないみたいだけど、どうしたの？[l]

[cm]

[chara_mod name="akane"  face="normal"]
#まどか
そろそろ焼けたかなって……[l]

[cm]

[chara_mod name="hi"  face="normal"]
#ほむら
そうね、もう少し時間が掛かると思うわ[l]

[cm]

@layopt layer=message0 visible=false

[chara_mod name="akane"  face="happy"]
[anim name=akane top="+=10" effect="easeInOutQuint" time=600]
[wait time=600]
[anim name=akane top="-=10" effect="easeOutQuint" time=600]
[wait time=800]

[wa]

@layopt layer=message0 visible=true

#まどか
分かった……[l]

[cm]

[mask time=1000]
[chara_mod name="akane"  face="normal"]
[chara_mod name="hi"  face="happy"]
[mask_off time=1000]

#ほむら
うん、いい感じ！[l]

[cm]

@layopt layer=message0 visible=false

[anim name=hi left="-=10" effect="easeInOutQuint" time=600]
[wait time=600]
[anim name=hi left="+=10" effect="easeOutQuint" time=600]
[wait time=800]

[wa]

@layopt layer=message0 visible=true

#ほむら
はい、どうぞ[l]

[cm]

@layopt layer=message0 visible=false

[chara_mod name="akane"  face="happy"]
[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[wait time=400]

[wa]

@layopt layer=message0 visible=true

#ほむら
それじゃあ[l]

[cm]

@layopt layer=message0 visible=false

[anim name=akane top="-=10" time=100]
[anim name=hi top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[anim name=hi top="+=10" time=100]
[wait time=100]
[anim name=akane top="-=10" time=100]
[anim name=hi top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[anim name=hi top="+=10" time=100]

[wait time=200]

[wa]

@layopt layer=message0 visible=true

#まどか・ほむら
「「いっただきまーす！」」[l]

[cm]

@layopt layer=message0 visible=false

[camera x=-150 y=40 zoom=1.7 time=500]
[chara_mod name="akane"  face="sad"]
[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[wait time=100]

[wa]

@layopt layer=message0 visible=true

#まどか
……う、うぅ～！！！！！[l]

[cm]

@layopt layer=message0 visible=false

[camera x=200 y=40 zoom=1.7 time=500]

[wa]

@layopt layer=message0 visible=true

[chara_mod name="hi"  face="doki"]
#ほむら
……熱いって！？[l]

[cm]

@layopt layer=message0 visible=false

[chara_mod name="hi"  face="angry"]
[anim name=hi top="-=10" time=100]
[wait time=100]
[anim name=hi top="+=10" time=100]
[wait time=100]
[anim name=hi top="-=10" time=100]
[wait time=100]
[anim name=hi top="+=10" time=100]

[wait time=200]

[wa]

@layopt layer=message0 visible=true

#ほむら
大変、お水お水！[l]
[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]

