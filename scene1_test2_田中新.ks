*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time=0]


[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="35" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]

[chara_config ptext="chara_name_area"]

;————————ここから————————

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]

;キャラ２定義
[chara_new name="aoi" storage="chara/aoi/normal.png" jname="あおい"]

;キャラ１表情
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]

;キャラ２表情
[chara_face name="aoi" face="normal" storage="chara/aoi/normal.png" jname="あおい"]
[chara_face name="aoi" face="happy" storage="chara/aoi/happy.png" jname="あおい"]
[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png" jname="あおい"]
[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png" jname="あおい"]
[chara_face name="aoi" face="angry" storage="chara/aoi/angry.png" jname="あおい"]

;背景
[image layer=0 folder="bgimage" name="gaikan" storage="pool.jpg" visible=true top=-100 left=0 width=1200 height=675]
[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="350" left="0" time="100" wait=false]
[image layer="1" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="-945" left="0" time="100" wait=true]
[camera x=0 y=0 zoom=2.0 time=0]

;メニューボタン
@showmenubutton

[mask_off time=1000]
[wait time=200]

[chara_mod name="akane"  face="angry"]
[chara_show name="akane" left=280 top=130]
[playbgm storage="dozikkomarch.ogg"]

@layopt layer=message0 visible=true

#あかね　
あおいちゃん[r]
海まで来て制服って、ないと思うんだ……[l]

[cm]

[mask time=500]
[anim name=akane left="+=550" time=0 effect="easeInSine"]
;あかね、画面外に退場
[chara_mod name="aoi"  face="normal"]
[chara_show name="aoi" left=280 top=50]
;あおい表示
[mask_off time=500]

#あおい
なに言ってるの？[r]
学生はどんな時でも制服を着用するものよ！[l]

[cm]

@layopt layer=message0 visible=false

[anim name=aoi top="+=10" effect="easeInOutQuint" time=300]
[wait time=300]
[anim name=aoi top="-=10" effect="easeOutQuint" time=300]
[wait time=400]

[wa]

@layopt layer=message0 visible=true

#あおい
あかねも制服に着替えましょう！[l]

[cm]

[mask time=500]
[anim name=akane left="-=550" time=0 effect="easeInSine"]
;あかね、画面外から戻す
[anim name=aoi left="+=550" time=0 effect="easeInSine"]
;あおい画面外に飛ばす
[mask_off time=500]

#あかね
やだよ……、私は海で泳いてくる！[l]

[cm]

@layopt layer=message0 visible=false

[wait time=200]
[chara_mod name="akane" face="angry" reflect="true" ]

[wa]

@layopt layer=message0 visible=true

#あかね
じゃあね、あおいちゃん！[l]

[cm]

@layopt layer=message0 visible=false
[anim name=akane left="+=550" time=1000 effect="easeInSine"]
[wait time=200]
[chara_hide name=akane" ]
[wait time=300]

[mask time=250]
[freeimage layer=0 time=0]
[freeimage layer=1 time=0]
[reset_camera]
[image layer="0" folder="bgimage" name="nakaniwa_yu" storage="living.jpg" visible="true" top="0" left="0" width=1200 height=675]
[chara_mod name="akane"  face="sad"]
[chara_mod name="aoi"  face="angry"]
[chara_show name="akane" left=280 top=60]
[chara_show name="aoi" left=0 top=60]
[mask_off time=250]

[wait time=200]

[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]
[wait time=100]
[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]

[wait time=200]

[wa]

@layopt layer=message0 visible=true

#あおい
あかね、起きて！[r]
こんなところで寝たら風邪ひくよ！[l]

[cm]

@layopt layer=message0 visible=false

[anim name=akane top="+=10" effect="easeInOutQuint" time=600]
[wait time=600]
[anim name=akane top="-=10" effect="easeOutQuint" time=600]
[wait time=800]

[wa]

@layopt layer=message0 visible=true

#あかね
……うぅ……寒い……――っくしゅん！[l]

[cm]

[chara_mod name="aoi"  face="sad"]
#あおい
はぁ……[r]
風邪ひいたんじゃない？[l]

[cm]

[chara_mod name="aoi"  face="angry"]
#あおい
ちょっとこっちに来て[r]
おでこ出してみて[l]

[cm]

@layopt layer=message0 visible=false

[anim name=akane left="-=10" time=400]
[wait time=400]
[anim name=akane left="+=10" time=400]
[wait time=400]
[anim name=akane left="-=10" time=400]
[wait time=400]
[anim name=akane left="+=10" time=400]

[wait time=400]
[chara_mod name="akane"  face="normal"]

[wa]

@layopt layer=message0 visible=true

#あかね
え？……あれ……私の海は？[l]

[cm]

@layopt layer=message0 visible=false

[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]
[wait time=100]
[anim name=aoi top="-=10" time=100]
[wait time=100]
[anim name=aoi top="+=10" time=100]

[wait time=200]

[wa]

@layopt layer=message0 visible=true

#あおい
なに訳のわからないこと言ってるの！[r]
いいからおでこを出す！[l]

[cm]

@layopt layer=message0 visible=false

[anim name=aoi left="+=100" time=500 effect="easeInSine"]

[wa]

@layopt layer=message0 visible=true

[chara_mod name="akane"  face="doki"]
#あかね
あ……[l]

[cm]

@layopt layer=message0 visible=false
[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[wait time=100]
@layopt layer=message0 visible=true

#あかね
……えっと、それは恥ずかしいよ～[l]

[cm]

[chara_mod name="aoi"  face="sad"]
#あおい
いいから！[r]
うーん、ちょっと熱っぽい気もするけど……[l]

[cm]

[chara_mod name="aoi"  face="angry"]
#あおい
もう少ししっかりおでこかしなさいよ[l]

[cm]

@layopt layer=message0 visible=false

[anim name=aoi left="+=10" time=100]
[anim name=akane left="+=5" time=100]
[wait time=100]
[anim name=aoi left="-=10" time=100]
[anim name=akane left="-=5" time=100]
[wait time=100]
[anim name=aoi left="+=10" time=100]
[anim name=akane left="+=5" time=100]
[wait time=100]
[anim name=aoi left="-=10" time=100]
[anim name=akane left="-=5" time=100]
[wait time=200]

[wa]

@layopt layer=message0 visible=true

[chara_mod name="aoi"  face="sad"]
#あおい
うーん、やっぱり少しありそうな感じね……[l]

[cm]

@layopt layer=message0 visible=false

[chara_mod name="akane"  face="angry"]
[wait time=200]
[chara_mod name="akane" face="angry" reflect="true" ]
[wait time=200]
[anim name=akane left="+=150" time=200 effect="easeInSine"]
[wait time=300]
[chara_mod name="akane" face="angry" reflect="false"]
[wa]

@layopt layer=message0 visible=true


#あかね
だ、大丈夫だから！[r]
そろそろ、お風呂入って寝るから！[l]

[cm]

@layopt layer=message0 visible=false

[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
[wait time=100]
[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]

[wait time=200]

[wa]

@layopt layer=message0 visible=true

#あかね
あおいちゃんも風邪ひかないように気を付けてね！[l]
[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]

